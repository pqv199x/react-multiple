import React from 'react';
import '../css/App.css';
import logo from './logo.svg';
import CheckLogin from './CheckLogin';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state ={
      userInfo: '',
      username: '',
      password: '',
    };
  }

  checkSubmit() {
    this.setState({
      userInfo: {
        username: this.state.username,
        password: this.state.password
      }
    });
    // let user = this.state.username;
    // let pass = this.state.password;
    // const { history } = this.props;
    // if (user === 'admin' && pass === '1234') {
    //   history.push('/home');
    // } else {

    // }
  }

  render() {
    return (
      <div>
        <div className="App">
          <div className="image">
            <img src={logo} className="App-logo" alt="logo" />
          </div>
          <CheckLogin user={this.state.userInfo}/>
          <input
            className="Login-Input"
            placeholder="username here..."
            type="text"
            autoFocus="autofocus"
            onChange={(event) => this.setState({username: event.target.value})}
          />
          <br />
          <input
            className="Login-Input"
            placeholder="password here..."
            type="password"
            onChange={(event) => this.setState({password: event.target.value})}
            onKeyPress={(event) => {
              if (event.key === 'Enter') {
                this.checkSubmit();
              }
            }}
          />
          <div>
          <button
              type="Submit"
              onClick={() => this.checkSubmit()}
          >OK</button>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginForm;
