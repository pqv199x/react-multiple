import React, { Component } from 'react';
import '../css/App.css';
import './style.css';

class MusicMaster extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
    };
  }
  search() {
    console.log();
  }
  render() {
    return (
      <div className="Music-Master">
        <div>
          <input placeholder="search an artist..."
            query={this.state.query}
            onChange={(event) => this.setState({query: event.target.value})}
            onKeyPress={(event) => {
              if (event.key === 'Enter') {
                this.search();
              }
            }}
          />
          <button onClick={() => this.search()}>Search</button>
        </div>
        <div className="Profile">
          <div>Artist name</div>
          <div>Artist name</div>
        </div>
        <div className="Gallery">
          Gallery
        </div>
      </div>
    );
  }
}

export default MusicMaster
