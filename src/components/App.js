import React, { Component } from 'react';
import './css/App.css';
import './css/nav.css';
import LoginForm from './Login/LoginForm';
import HomePage from './HomePage';
import About from './About';
import MusicMaster from './MusicMaster/MusicMaster';
import CountdownClock from './CountDownClock/CountdownClock';
import { HashRouter, Route, Switch } from 'react-router-dom';


class App extends Component {
  render() {
    return (
      <div>
        <nav>
        <ul>
          <li><a class="active" href="#home">Home</a></li>
          <li><a href="#countdown">Count down clock</a></li>
          <li><a href="#news">News</a></li>
          <li><a href="#contact">Contact</a></li>
          <li><a href="#musicmaster">Music Master</a></li>
          <li><a href="#about">About</a></li>
        </ul>
        </nav>
          {renderRouters()}
      </div>
    );
  }
}

function renderRouters(user) {
  const requireAuthen = (comp) => user == null ? redirect('/login') : comp;
  const requireUnauthen = (comp) => user !==null ? redirect('/home') : comp;


  return (
    <HashRouter>
      <Switch>>
        <Route exact path='/' component={redirect(user ? '/home' : '/login')} />
        <Route exact path='/login' component={LoginForm} />
        <Route exact path='/home' component={HomePage} />
        <Route exact path='/countdown' component={CountdownClock} />
        <Route exact path='/musicmaster' component={MusicMaster} />
        <Route exact path='/about' component={About} />
      </Switch>
    </HashRouter>
  );
}

function redirect(location) {
  return class RedirectRoute extends Component {
    constructor(props) {
      super(props);
      props.history.push(location);
    }
    render() {
      return null;
    }
  }
}

export default App;
