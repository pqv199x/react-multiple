import React from 'react';
import '../css/App.css';
import CountdownHandler from './CountdownHandler';

class CountdownClock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deadline: 'December 25, 2018',
      newDeadline: ''
    };
  }
  changeDeadline() {
    this.setState({deadline: this.state.newDeadline});
  }
  render() {
    return (
      <div className="App">
        <div className="App-title">Countdown to {this.state.deadline}</div>
        <div>
          <CountdownHandler
            deadline={this.state.deadline}
           />
        </div>
        <div>
          <input
            placeholder="Enter date..."
            onChange={(event) => this.setState({newDeadline: event.target.value})}
          />
          <button onClick={() => this.changeDeadline()}>Enter</button>
        </div>
      </div>
    );
  }
}

export default CountdownClock;